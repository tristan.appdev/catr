use clap::Parser;
use std::{
    fs::File,
    io::{BufRead, BufReader, Error},
    iter::Iterator,
};

/// Type alias `BoxedStrIter` which represents a `'static` `std::iter::Iterator<Item = &str>` with a `Box`.
/// This is used to avoid the `std::iter::Iterator` lifetime error.
///
/// # Example
/// ```
///
/// ```
type BoxedStrIter = Box<dyn Iterator<Item = String> + 'static>;

/// Represents the arguments passed to the program.
/// # Examples
/// ```
/// use clap::Parser;
/// ...
/// let args = Args::parse();
/// let number: bool = args.number;
/// let file_nammes: Vec<String> = args.file_names;
/// ```
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about=None)]
struct Args {
    #[clap(short = 'n', long = "number")]
    number: bool,

    #[clap(short = 'b', long = "number-nonblank")]
    number_non_blank: bool,

    #[clap(short = 'E', long = "show-ends")]
    show_ends: bool,

    #[clap()]
    file_names: Vec<String>,
}

/// Main Function of the program.
/// # Examples
/// ```
///
/// ```
fn main() {
    let args: Args = Args::parse();

    for file_name in args.file_names.to_owned() {
        let lines: BoxedStrIter = match read_file(file_name.clone()) {
            Err(e) => {
                handle_error(&e);
                continue;
            }
            Ok(lines) => lines,
        };
        let lines: BoxedStrIter = apply_args(lines, &args);
        print_to_console(lines);
    }
}

/// Reads a file and returns a Result<dyn Iterator<Item = String> + 'static, std::io::Error>.
///  # Arguments
/// * `file_name` - The name of the file to read.
fn read_file(file_name: String) -> Result<BoxedStrIter, Error> {
    let file: File = match File::open(&file_name) {
        Err(e) => return Err(e),
        Ok(file) => file,
    };

    Ok(Box::new(
        BufReader::new(file).lines().map(|line| line.unwrap()),
    ))
}

/// Changes the lines, depending on which arguments were used.
/// # Arguments
/// * `lines` - The lines to change.
/// * `args` - The arguments passed to the program.
/// # Returns
/// A `BoxedStrIter` containing the changed lines.
fn apply_args(lines: BoxedStrIter, args: &Args) -> BoxedStrIter {
    let mut changed_lines: BoxedStrIter = if args.show_ends {
        show_ends(lines)
    } else {
        Box::new(lines)
    };

    changed_lines = if args.number_non_blank {
        number_b(changed_lines)
    } else if args.number {
        number_n(changed_lines)
    } else {
        changed_lines
    };

    changed_lines
}

/// Numbers the lines (including blank lines).
/// # Arguments
/// * `lines` - The lines to number.
/// # Returns
/// A `BoxedStrIter` containing the numbered lines.
/// # Examples
/// ```
///
/// ```
fn number_n(lines: BoxedStrIter) -> BoxedStrIter {
    Box::new(
        lines
            .into_iter()
            .enumerate()
            .map(|(index, line)| format!("{}{}{}", index + 1, "  ", line)),
    )
}

/// Numbers the lines (excluding blank lines).
/// # Arguments
/// * `lines` - The lines to number.
/// # Returns
/// A `BoxedStrIter` containing the numbered lines.
/// # Examples
/// ```
///
/// ```
fn number_b(lines: BoxedStrIter) -> BoxedStrIter {
    let mut counter: u32 = 0;

    Box::new(lines.into_iter().map(move |line| {
        let trimmed_line: &str = line.trim();
        if trimmed_line.is_empty() || trimmed_line == "$" {
            line
        } else {
            counter += 1;
            format!("{counter}    {line}")
        }
    }))
}

/// Adds the end of line characters to the lines.
/// # Arguments
/// * `lines` - The lines to add the end of line characters to.
/// # Returns
/// A `BoxedStrIter` containing the lines with the end of line characters.
/// # Examples
/// ```
///
/// ```
fn show_ends(lines: BoxedStrIter) -> BoxedStrIter {
    Box::new(lines.into_iter().map(|line| format!("{}{}", line, "$")))
}

/// Prints the lines to the console.
/// # Arguments
/// * `lines` - The lines to print.
/// # Examples
/// ```
///
/// ```
fn print_to_console(lines: BoxedStrIter) {
    lines.into_iter().for_each(|line| println!("{}", line));
}

/// Handles an error and prints it to the console.
/// # Arguments
/// * `error` - The error to be handled.
/// # Examples
/// ```
///
/// ```
fn handle_error(error: &Error) {
    println!("{}", error.to_string());
}
